///<reference path="../../docs/jquery.d.ts"/>
class LotteryApp {

    public run(): void {
        console.log('app started');

        $('button.try-luck').click(function(){
            let $this = $(this);
            $.ajax('/try-luck',{
                method: 'POST'
            }).then(function(result){
                $('div.won-prize').html(result);
            })
        });

    }

}

$(function() {
    let app = new LotteryApp();
    app.run();
});