///<reference path="../../docs/jquery.d.ts"/>
var LotteryApp = /** @class */ (function () {
    function LotteryApp() {
    }
    LotteryApp.prototype.run = function () {
        console.log('app started');
        $('button.try-luck').click(function () {
            var $this = $(this);
            $.ajax('/try-luck', {
                method: 'POST'
            }).then(function (result) {
                $('div.won-prize').html(result);
            });
        });
    };
    return LotteryApp;
}());
$(function () {
    var app = new LotteryApp();
    app.run();
});
