<?php

try {
    require __DIR__.'/../bootstrap.php';

    $app = new \Lottery\Logic\LotteryApp;
    Context::setApp($app);

    $router = new \Api\Router;
    $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    $response = $router->handleRequest($request);
    Context::getSession()->save();
    $response->send();
} catch (\Throwable $t) {
    echo '<pre>Something went terribly wrong: '.PHP_EOL.$t.'</pre>';
}