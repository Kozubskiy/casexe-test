<?php

require_once __DIR__.'/bootstrap.php';

$console = new \Symfony\Component\Console\Application('Lottery CLI');

$commandList = [
    'send-money' => Command\SendMoneyPrizesCommand::class,
    'add-user' => Command\AddUserCommand::class,
];

foreach($commandList as $commandName => $commandClass ) {
    try {
        $console->add( new $commandClass($commandName) );
    } catch ( \Throwable $e ) {
        echo "\nSomething went wrong: ".$e->getMessage();
    }
}
/** @noinspection PhpUnhandledExceptionInspection */
$console->run();
