<?php
/** @var \Lottery\Entity\MoneyPrize $prize */
?>
<h1>Вы выиграли деньги!</h1>
<img src="/img/money.png" />
<h2>Количество: €<?= $prize->getAmount() ?></h2>
<div class="options">
    <button>Перечислить на счет</button>
    <button>Конвертировать в баллы</button>
    <button>Отказаться</button>
</div>