<?php
/** @var \Lottery\Entity\ThingPrize $prize */
?>
<h1>Вы выиграли вещь <b><?= $prize->getThing()->getTitle() ?>!</b></h1>
<img src="<?= $prize->getThing()->getPicturePath() ?>" />
<div class="options">
    <button>Получить</button>
    <button>Отказаться</button>
</div>