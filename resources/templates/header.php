<?php
/** @var string $_content */
/** @var \Lottery\Entity\User $user */
/** @var \Api\TemplateRenderer $this */
$this->extendsTemplate('html-wrapper');
?>
<div class="header">
    <div class="navigation">
        <a href="/">Розыгрыш</a>
        <a href="/prizes">Мои призы</a>
        <a href="/logout">Выйти</a>
    </div>
    <div>Мои баллы лояльности: <?= $user->getCurrentBonusBalance() ?></div>
    <div>Мои деньги: €<?= $user->getCurrentMoneyBalance() ?></div>
</div>
<?= $_content ?>