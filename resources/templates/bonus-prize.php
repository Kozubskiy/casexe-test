<?php
/** @var \Lottery\Entity\BonusPrize $prize */
?>
<h1>Вы выиграли <?= $prize->getAmount() ?> баллы(ов) лояльности!</h1>
<?php if ($prize->getState() === \Lottery\Entity\BonusPrize::STATE_WAITING_FOR_BALANCE_CHARGING): ?>
<p>Они уже ожидают зачисления на Ваш баланс</p>
<?php else: ?>
<p>Они уже зачислены на Ваш баланс</p>
<?php endif; ?>