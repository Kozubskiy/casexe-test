<?php
/** @var \Api\TemplateRenderer $this */
$this->extendsTemplate('html-wrapper');
?>
<form action="/login" method="post">
    <input name="login" placeholder="login" value="<?= \getenv('DEV_USER_LOGIN') ?>" />
    <input name="password" type="password" value="<?= \getenv('DEV_USER_PASSWORD') ?>" placeholder="password" />
    <input type="submit">
</form>