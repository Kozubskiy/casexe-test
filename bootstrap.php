<?php /** @noinspection PhpUnhandledExceptionInspection */

use Doctrine\Common\Persistence\Mapping\Driver\PHPDriver;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Lottery\Database\Types\BigIntCastingToIntType;
use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__.'/vendor/autoload.php';
const TEMPLATES_PATH = __DIR__.'/resources/templates';
Context::setRootPath(__DIR__);

// todo: setup database connection and doctrine entity manager
(new Dotenv())->load(__DIR__.'/.env');

// to cast bigint to int, not default string (as it can be useful only for x32 systems
Type::overrideType( Type::BIGINT, BigIntCastingToIntType::class );

$connection = \Doctrine\DBAL\DriverManager::getConnection(['url' => \getenv('DATABASE_URL')]);
$config = Setup::createConfiguration(false,__DIR__.'/var/doctrine-proxies',null);
$config->setMetadataDriverImpl( new PHPDriver(__DIR__.'/src/Lottery/Database/Mapping') );
$entityManager = EntityManager::create($connection, $config);
Context::setEntityManager($entityManager);
