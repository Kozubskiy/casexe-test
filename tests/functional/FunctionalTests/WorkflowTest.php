<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 11:40
 */

namespace FunctionalTests;

use Lottery\Entity\Lottery;
use Lottery\Entity\Thing;
use Lottery\Entity\User;
use Lottery\Operation\RegisterUserOperation;
use PHPUnit\Framework\TestCase;

class WorkflowTest extends TestCase
{

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function testWorkflow()
    {

        $em = \Context::getEntityManager();
        $app = \Context::getApp();

        // create new lottery
        $lottery = (new Lottery)
            ->setMoneyAmount(100)
            ->setBonusAmount(1000)
            ->setTitle('Test lottery '.time());

        /** @var Thing[] $things */
        $things = [];

        // add some things types
        for ($i = 1; $i <= 5; $i++) {
            $things[] = (new Thing)
                ->setPicturePath('/img/prize.jpg')
                ->setTitle('Boots');
        }

        // add some things to lottery
        for ($i = 1; $i <= 20; $i++) {
        }

        // register new user
        $user = (new RegisterUserOperation)
            ->setLogin('user'.time())
            ->setPassword('password')
            ->execute();

        $prize = $app->getRandomePrize();

    }

}
