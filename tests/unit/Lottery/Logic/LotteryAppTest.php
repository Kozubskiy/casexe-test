<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 11:40
 */

namespace Lottery\Logic;

use Lottery\Constants;
use Lottery\Entity\AbstractPrize;
use PHPUnit\Framework\TestCase;

class LotteryAppTest extends TestCase
{

    public function testGetRandomePrize()
    {
        $lotteryApp = new LotteryApp;
        $prize = $lotteryApp->getRandomePrize();
        $this->assertTrue(($prize === null) || ($prize instanceof AbstractPrize));
    }

    public function testConvertMoneyToBouns()
    {
        $lotteryApp = new LotteryApp;
        $moneyAmount = 100;
        $bonus = $lotteryApp->convertMoneyToBouns($moneyAmount);
        $this->assertEquals(Constants::MONEY_TO_BONUS_CONVERSION_RATE * $moneyAmount, $bonus);
    }

    public function converterDataProvider()
    {
        return [
            ['money' => 0, 'bonuses' => 0],
            ['money' => 100, 'bonuses' => 300],
            ['money' => 200, 'bonuses' => 600],
            ['money' => 300, 'bonuses' => 900],
        ];
    }

    /**
     * @dataProvider converterDataProvider
     */
    public function testConvertMoneyToBouns2($money, $expectedBonuses)
    {
        $lotteryApp = new LotteryApp;
        $moneyAmount = $money;
        $bonus = $lotteryApp->convertMoneyToBouns($moneyAmount);
        $this->assertEquals($expectedBonuses, $bonus);
    }

}
