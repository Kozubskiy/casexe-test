<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 17:29
 */

namespace Api;

use Action;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Router
{

    /**
     * @param Request $request
     *
     * @return Response
     * @throws NotAuthorizedException
     */
    public function handleRequest(Request $request): Response
    {
        $router = new \TreeRoute\Router();

        $router->addRoute('GET', '/', Action\IndexAction::class);
        $router->addRoute('GET', '/prizes', Action\PrizesAction::class);
        $router->addRoute(['GET','POST'], '/logout', Action\LogoutAction::class);
        $router->addRoute('POST', '/login', Action\LoginAction::class);
        $router->addRoute('POST', '/try-luck', Action\TryLuckAction::class);

        $method = $request->getMethod();
        $url = $request->getPathInfo();

        $result = $router->dispatch($method, $url);
        if (!isset($result['error'])) {
            $handlerClass = $result['handler'];
            /** @var Action\AbstractAction $handler */
            $handler = new $handlerClass;
            $params = $result['params'];
            $handler->setParams($params);
            $handler->setRequest($request);
            try {
                return $handler();
            } catch (NotAuthorizedException $t) {
                return new \Symfony\Component\HttpFoundation\RedirectResponse('/');
            }
            // Do something with handler and params
        } else {
            switch ($result['error']['code']) {
                case 404 :
                    // Not found handler here
                    break;
                case 405 :
                    // Method not allowed handler here
                    $allowedMethods = $result['allowed'];
                    if ($method === 'OPTIONS') {
                        // OPTIONS method handler here
                    }
                    break;
            }
        }

    }

}