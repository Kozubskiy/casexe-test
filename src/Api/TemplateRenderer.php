<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 17:44
 */

namespace Api;


class TemplateRenderer
{

    private $parentsStack = [];

    public function extendsTemplate(string $templateName): void
    {
        array_unshift($this->parentsStack,$templateName);
    }

    public function renderPhpTemplate( $_file_, array $_params_ = [] ): string
    {
        //\LE()->logger()->debug('Rendering component PHP template file: '.$_file_);

        if ( !\file_exists($_file_) ) {
            throw new \RuntimeException("Template file {$_file_} does not exists!");
        }

        ob_start();
        ob_implicit_flush(false);
        extract($_params_, EXTR_OVERWRITE);
        //$_context = new TemplateContext;
        try {

            /** @noinspection PhpIncludeInspection */
            require $_file_;

        } catch (\Throwable $e) {
            //\LE()->logger()->error("Error while rendering template {$_file_}:{$e->getLine()} {$e->getMessage()}");
            ob_clean();
        }

        $output = ob_get_clean();

        if ( $wrapperFile = array_shift($this->parentsStack) ) {
            $wrapperFile = TEMPLATES_PATH.'/'.$wrapperFile.'.php';
            $output = $this->renderPhpTemplate( $wrapperFile, ['_content' => $output ] + $_params_ );
        }

        return $output;

    }

}