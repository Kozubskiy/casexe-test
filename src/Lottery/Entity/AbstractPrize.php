<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:03
 */

namespace Lottery\Entity;


abstract class AbstractPrize
{

    use StaticRepoAccessTrait;

    /** @var int */
    protected $id;

    /** @var User */
    protected $user;

    /** @var int */
    protected $state = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $state
     *
     * @return static
     */
    public function setState(int $state): AbstractPrize
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param User $user
     *
     * @return static
     */
    public function setUser(User $user): AbstractPrize
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}