<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:00
 */

namespace Lottery\Entity;


class Lottery
{

    use StaticRepoAccessTrait;

    /** @var string */
    private $title;

    /** @var int */
    private $moneyAmount;

    /** @var int */
    private $bonusAmount;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Lottery
     */
    public function setTitle(string $title): Lottery
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return int
     */
    public function getMoneyAmount(): int
    {
        return $this->moneyAmount;
    }

    /**
     * @param int $moneyAmount
     *
     * @return Lottery
     */
    public function setMoneyAmount(int $moneyAmount): Lottery
    {
        $this->moneyAmount = $moneyAmount;
        return $this;
    }

    /**
     * @return int
     */
    public function getBonusAmount(): int
    {
        return $this->bonusAmount;
    }

    /**
     * @param int $bonusAmount
     *
     * @return Lottery
     */
    public function setBonusAmount(int $bonusAmount): Lottery
    {
        $this->bonusAmount = $bonusAmount;
        return $this;
    }

}