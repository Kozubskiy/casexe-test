<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:02
 */

namespace Lottery\Entity;

use Lottery\Database\Repository\MoneyPrizeRepository;

/**
 * @method static MoneyPrizeRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 */
class MoneyPrize extends AbstractPrize
{

    // набор стейтов отражает жизненный цикл приза
    public const STATE_WAITING_FOR_DECISION = 1; // полученный приз ожидает решения пользователя
    public const STATE_CANCELLED = 2; // пользователь отказался от приза (final state)
    public const STATE_WAITS_FOR_SEND = 3; // приз ожидает отправки пользователю
    public const STATE_SENT = 4; // приз ожидает отправки пользователю
    public const STATE_CONVERTED = 5; // приз был сконвертирован в баллы лояльности (final state)

    /** @var int */
    private $amount;

    /**
     * @param int $amount
     *
     * @return MoneyPrize
     */
    public function setAmount(int $amount): MoneyPrize
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

}