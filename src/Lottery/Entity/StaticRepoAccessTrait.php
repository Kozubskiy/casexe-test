<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 23:28
 */

namespace Lottery\Entity;


trait StaticRepoAccessTrait
{
    /**
     * @param \Doctrine\ORM\EntityManagerInterface $em
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public static function repo(\Doctrine\ORM\EntityManagerInterface $em = null)
    {
        if ($em === null) {
            $em = \Context::getEntityManager();
        }

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $em->getRepository(static::class);
    }
}