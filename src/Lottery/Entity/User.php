<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:01
 */

namespace Lottery\Entity;

use Lottery\Database\Repository\UserRepository;

/**
 * @method static UserRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 */
class User
{

    use StaticRepoAccessTrait;

    /** @var int */
    private $id;

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    /** @var int */
    private $currentMoneyBalance;

    /** @var int */
    private $currentBonusBalance;

    /**
     * @return int
     */
    public function getCurrentMoneyBalance(): int
    {
        return $this->currentMoneyBalance;
    }

    /**
     * @param int $currentMoneyBalance
     *
     * @return User
     */
    public function setCurrentMoneyBalance(int $currentMoneyBalance): User
    {
        $this->currentMoneyBalance = $currentMoneyBalance;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentBonusBalance(): int
    {
        return $this->currentBonusBalance;
    }

    /**
     * @param int $currentBonusBalance
     *
     * @return User
     */
    public function setCurrentBonusBalance(int $currentBonusBalance): User
    {
        $this->currentBonusBalance = $currentBonusBalance;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     *
     * @return User
     */
    public function setLogin(string $login): User
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}