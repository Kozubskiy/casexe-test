<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:01
 */

namespace Lottery\Entity;


class Thing
{

    use StaticRepoAccessTrait;

    /** @var int */
    protected $id;

    /** @var string */
    private $title;

    /** @var string */
    private $picturePath;

    /**
     * @return string
     */
    public function getPicturePath(): string
    {
        return $this->picturePath;
    }

    /**
     * @param string $picturePath
     *
     * @return Thing
     */
    public function setPicturePath(string $picturePath): Thing
    {
        $this->picturePath = $picturePath;
        return $this;
    }

    /**
     * @param string $title
     *
     * @return Thing
     */
    public function setTitle(string $title): Thing
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}