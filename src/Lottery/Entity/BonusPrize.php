<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:02
 */

namespace Lottery\Entity;

use Lottery\Database\Repository\BonusPrizeRepository;

/**
 * @method static BonusPrizeRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 */
class BonusPrize extends AbstractPrize
{

    /** @var int */
    private $amount;

    public const STATE_WAITING_FOR_BALANCE_CHARGING = 1; // полученные баллы ожидают зачисления на баланс
    public const STATE_HANDLED = 2; // пользователь отказался от приза (final state)

    /**
     * @param int $amount
     *
     * @return BonusPrize
     */
    public function setAmount($amount): BonusPrize
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

}