<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:02
 */

namespace Lottery\Entity;

use Lottery\Database\Repository\ThingPrizeRepository;

/**
 * Class ThingPrize
 * Отправка призов могла бы быть организована через отдельную очередь задач по отправке.
 * @method static ThingPrizeRepository repo(\Doctrine\ORM\EntityManagerInterface $em = null)
 */
class ThingPrize extends AbstractPrize
{

    // набор стейтов отражает жизненный цикл приза
    public const STATE_WAITING_FOR_DECISION = 1; // полученный приз ожидает решения пользователя
    public const STATE_CANCELLED = 2; // пользователь отказался от приза (final state)
    public const STATE_WAITS_FOR_SEND = 3; // приз ожидает отправки пользователю
    public const STATE_SENT = 4; // приз ожидает отправки пользователю

    /** @var Thing */
    private $thing;

    /**
     * @param Thing $thing
     *
     * @return ThingPrize
     */
    public function setThing(Thing $thing): ThingPrize
    {
        $this->thing = $thing;
        return $this;
    }

    /**
     * @return Thing
     */
    public function getThing(): Thing
    {
        return $this->thing;
    }

}