<?php

/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:08
 */

namespace Lottery\Logic;


use Helper\Random;
use Lottery\Constants;
use Lottery\Entity;

class LotteryApp
{

    private function getRandomAvailableThing(): Entity\Thing
    {
        // todo: get random available thing
        return (new Entity\Thing)
            ->setPicturePath('/img/prize.jpg')
            ->setTitle('Very useful thing');
    }

    public function getRandomePrize(): ?Entity\AbstractPrize
    {
        $luck = Random::getRandomInt(0,100);

        if ($luck <= Constants::PRIZE_PROBABILITY_THING) {
            // todo: thing prizes must be available
            $randomThing = $this->getRandomAvailableThing();
            return (new Entity\ThingPrize)
                ->setState(Entity\ThingPrize::STATE_WAITING_FOR_DECISION)
                ->setThing($randomThing);
        }

        if ($luck <= Constants::PRIZE_PROBABILITY_MONEY) {
            // todo: check money must be available
            $amount = Random::getRandomInt(Constants::PRIZE_MONEY_MIN,Constants::PRIZE_MONEY_MAX);
            return (new Entity\MoneyPrize)
                ->setState(Entity\MoneyPrize::STATE_WAITING_FOR_DECISION)
                ->setAmount($amount);
        }

        if ($luck <= Constants::PRIZE_PROBABILITY_BONUS) {
            $amount = Random::getRandomInt(Constants::PRIZE_BONUS_MIN,Constants::PRIZE_BONUS_MAX);
            return (new Entity\BonusPrize)
                ->setAmount($amount)
                ->setState(Entity\BonusPrize::STATE_WAITING_FOR_BALANCE_CHARGING);
        }

        // no luck
        return null;

    }

    public function convertMoneyToBouns(int $moneyAmount): int
    {
        return Constants::MONEY_TO_BONUS_CONVERSION_RATE * $moneyAmount;
    }

}