<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 23:25
 */

namespace Lottery\Logic;


class Security
{
    public static function hashPassword(string $password): string
    {
        return \password_hash($password, \PASSWORD_BCRYPT);
    }

    public static function isPasswordCorrect(string $password, string $hash): bool
    {
        return \password_verify($password, $hash);
    }
}