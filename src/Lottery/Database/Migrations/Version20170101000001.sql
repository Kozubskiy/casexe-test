CREATE TABLE users (
  id BIGSERIAL PRIMARY KEY,
  login VARCHAR(100) NOT NULL,
  password VARCHAR(100) NOT NULL,
  current_money_balance INTEGER DEFAULT 0 NOT NULL,
  current_bonus_balance INTEGER DEFAULT 0 NOT NULL,
  UNIQUE (login)
);

COMMENT ON TABLE users IS 'Пользователи';
COMMENT ON COLUMN users.id IS 'Целочисленный идентификатор Пользователя';
COMMENT ON COLUMN users.login IS 'Логин Пользователя';
COMMENT ON COLUMN users.password IS 'Хэш пароля Пользователя';
COMMENT ON COLUMN users.current_money_balance IS 'Текущий баланс денег Пользователя';
COMMENT ON COLUMN users.current_bonus_balance IS 'Текущий баланс бонусов лояльности Пользователя';

-- типы призов-вещей
CREATE TABLE things (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(200),
  picture_path VARCHAR(512)
);

CREATE TABLE money_prizes (
  id BIGSERIAL PRIMARY KEY,
  state SMALLINT,
  amount INTEGER CHECK (amount > 0),
  user_id BIGINT REFERENCES users NOT NULL
);

CREATE TABLE thing_prizes (
  id BIGSERIAL PRIMARY KEY,
  state SMALLINT,
  thing_id BIGINT REFERENCES things NOT NULL,
  user_id BIGINT REFERENCES users NOT NULL
);

CREATE TABLE bonus_prizes (
  id BIGSERIAL PRIMARY KEY,
  state SMALLINT,
  amount INTEGER CHECK (amount > 0),
  user_id BIGINT REFERENCES users NOT NULL
);

CREATE TABLE lotteries (
  id BIGSERIAL PRIMARY KEY,
  title VARCHAR(200),
  money_amount INTEGER,
  bonus_amount INTEGER
);

-- неразыгранные призы-вещи в лотерее
CREATE TABLE lottery_things_balance (
  id BIGSERIAL PRIMARY KEY,
  lottery_id BIGINT REFERENCES lotteries NOT NULL,
  thing_id BIGINT REFERENCES things NOT NULL,
  balance INTEGER DEFAULT 0 NOT NULL CHECK (balance >= 0)
);