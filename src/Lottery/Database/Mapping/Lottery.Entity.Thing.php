<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/** @var $metadata Doctrine\ORM\Mapping\ClassMetadataInfo */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setTable('things');

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

$mapper->createField('title', Type::STRING)
    ->columnName('title')
    ->build();

$mapper->createField('picturePath', Type::STRING)
    ->columnName('picture_path')
    ->build();