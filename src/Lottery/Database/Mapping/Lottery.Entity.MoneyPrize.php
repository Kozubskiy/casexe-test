<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/** @var $metadata Doctrine\ORM\Mapping\ClassMetadataInfo */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setTable('money_prizes');

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

$mapper->createField('state', Type::SMALLINT)
    ->columnName('state')
    ->build();

$mapper->createField('amount', Type::INTEGER)
    ->columnName('amount')
    ->build();

$mapper->createManyToOne('user', Lottery\Entity\User::class)
    ->addJoinColumn('user_id', 'id')
    //->cascadePersist()
    ->cascadeRefresh()
    ->fetchLazy()
    ->build();