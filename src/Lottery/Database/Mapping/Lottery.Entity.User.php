<?php

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Mapping\Builder\ClassMetadataBuilder;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

/** @var $metadata Doctrine\ORM\Mapping\ClassMetadataInfo */

/** @noinspection PhpUnhandledExceptionInspection */
$metadata->setInheritanceType(ClassMetadataInfo::INHERITANCE_TYPE_NONE);
$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_SEQUENCE);

$mapper = new ClassMetadataBuilder($metadata);
$mapper->setTable('users');

$mapper->createField('id', Type::BIGINT)
    ->columnName('id')
    ->makePrimaryKey()
    ->option('unsigned',false)
    ->build();

$mapper->createField('login', Type::STRING)
    ->columnName('login')
    ->build();

$mapper->createField('password', Type::STRING)
    ->columnName('password')
    ->build();

$mapper->createField('currentMoneyBalance', Type::INTEGER)
    ->columnName('current_money_balance')
    ->build();

$mapper->createField('currentBonusBalance', Type::INTEGER)
    ->columnName('current_bonus_balance')
    ->build();