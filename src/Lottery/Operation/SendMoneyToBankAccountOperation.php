<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 12:20
 */

namespace Lottery\Operation;


class SendMoneyToBankAccountOperation extends AbstractOperation
{

    /** @var int */
    private $amount;

    /** @var string let every bank in the world has unique ID */
    private $bankId;

    /** @var string let any account has unique string id through concrete bank */
    private $bankAccount;

    /**
     * @param int $amount
     *
     * @return SendMoneyToBankAccountOperation
     */
    public function setAmount(int $amount): SendMoneyToBankAccountOperation
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @param string $bankId
     *
     * @return SendMoneyToBankAccountOperation
     */
    public function setBankId(string $bankId): SendMoneyToBankAccountOperation
    {
        $this->bankId = $bankId;
        return $this;
    }

    /**
     * @param string $bankAccount
     *
     * @return SendMoneyToBankAccountOperation
     */
    public function setBankAccount(string $bankAccount): SendMoneyToBankAccountOperation
    {
        $this->bankAccount = $bankAccount;
        return $this;
    }

    /**
     * @return mixed|void
     */
    public function execute()
    {
        // get bank API SDK
        // TODO: send http request to bank API via curl/guzzle/etc
    }

}