<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:05
 */

namespace Lottery\Operation;


use Lottery\Entity\User;
use Lottery\Logic\Security;

class RegisterUserOperation extends AbstractOperation
{

    /** @var string */
    private $login;

    /** @var string */
    private $password;

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function execute()
    {
        $hashedPassword = Security::hashPassword($this->password);

        $user = (new User)
            ->setCurrentBonusBalance(0)
            ->setCurrentMoneyBalance(0)
            ->setLogin($this->login)
            ->setPassword($hashedPassword);

        \Context::getEntityManager()->persist($user);
        \Context::getEntityManager()->flush($user);
        return $user;
    }

    /**
     * @param string $password
     *
     * @return RegisterUserOperation
     */
    public function setPassword(string $password): RegisterUserOperation
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param string $login
     *
     * @return RegisterUserOperation
     */
    public function setLogin(string $login): RegisterUserOperation
    {
        $this->login = $login;
        return $this;
    }

}