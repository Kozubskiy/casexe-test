<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 12:25
 */

namespace Lottery\Operation;


use Lottery\Entity\BonusPrize;
use Lottery\Entity\MoneyPrize;

class ConvertMoneyPrizeToBonusPrizeOperation extends AbstractOperation
{

    /** @var MoneyPrize */
    private $sourceMoneyPrize;

    /**
     * @return BonusPrize
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(): BonusPrize
    {
        $lotteryApp = \Context::getApp();
        $moneyPrize = $this->sourceMoneyPrize;
        $moneyAmount = $moneyPrize->getAmount();
        $bonusAmount = $lotteryApp->convertMoneyToBouns($moneyAmount);
        $user = $moneyPrize->getUser();

        $bonusPrize = (new BonusPrize)
            ->setState(BonusPrize::STATE_WAITING_FOR_BALANCE_CHARGING)
            ->setUser($user)
            ->setAmount($bonusAmount);

        \Context::getEntityManager()->persist($bonusPrize);

        // remove source moneyPrize as it is already converted
        \Context::getEntityManager()->remove($this->sourceMoneyPrize);

        \Context::getEntityManager()->flush();


        return $bonusPrize;
    }

    /**
     * @param MoneyPrize $sourceMoneyPrize
     *
     * @return ConvertMoneyPrizeToBonusPrizeOperation
     */
    public function setSourceMoneyPrize(MoneyPrize $sourceMoneyPrize): ConvertMoneyPrizeToBonusPrizeOperation
    {
        $this->sourceMoneyPrize = $sourceMoneyPrize;
        return $this;
    }
}