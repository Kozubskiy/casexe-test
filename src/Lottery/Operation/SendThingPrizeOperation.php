<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 12:18
 */

namespace Lottery\Operation;


use Lottery\Entity\ThingPrize;

class SendThingPrizeOperation extends AbstractOperation
{

    /** @var ThingPrize */
    private $thingPrize;

    /**
     * @param ThingPrize $thingPrize
     *
     * @return SendThingPrizeOperation
     */
    public function setThingPrize(ThingPrize $thingPrize): SendThingPrizeOperation
    {
        $this->thingPrize = $thingPrize;
        return $this;
    }

    /**
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     */
    public function execute()
    {
        // todo: do some manual work to send thing to application user

        $this->thingPrize->setState(ThingPrize::STATE_SENT);
        \Context::getEntityManager()->persist($this->thingPrize);
        \Context::getEntityManager()->flush();

    }

}