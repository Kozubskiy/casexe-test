<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:04
 */

namespace Lottery\Operation;


abstract class AbstractOperation
{

    /**
     * @return mixed|void
     */
    abstract public function execute();
}