<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 12:25
 */

namespace Lottery\Operation;


use Lottery\Entity\BonusPrize;

class ChargeBonusBalanceByBonusPrizeOperation extends AbstractOperation
{

    /** @var BonusPrize */
    private $sourceBonusPrize;

    /**
     * @return BonusPrize
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function execute(): BonusPrize
    {
        $bonusPrize = $this->sourceBonusPrize;

        if (!$bonusPrize->getState() === BonusPrize::STATE_WAITING_FOR_BALANCE_CHARGING) {
            throw new \LogicException('Cannot charge bonus balance for user: bonus prize with ID = '.$bonusPrize->getId().' is already used to charge balance.');
        }

        $user = $bonusPrize->getUser();
        $bonusAmount = $bonusPrize->getAmount();
        $user->setCurrentBonusBalance( $user->getCurrentBonusBalance() + $bonusAmount);
        $bonusPrize->setState(BonusPrize::STATE_HANDLED);

        $em = \Context::getEntityManager();
        $em->persist($bonusPrize);
        $em->persist($user);
        $em->flush();

        return $bonusPrize;
    }

    /**
     * @param BonusPrize $sourceBonusPrize
     *
     * @return ChargeBonusBalanceByBonusPrizeOperation
     */
    public function setSourceBonusPrize(BonusPrize $sourceBonusPrize): ChargeBonusBalanceByBonusPrizeOperation
    {
        $this->sourceBonusPrize = $sourceBonusPrize;
        return $this;
    }
}