<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 13:04
 */

namespace Lottery\Operation;


use Lottery\Entity\MoneyPrize;

class SendMoneyPrizeOperation extends AbstractOperation
{

    /** @var MoneyPrize */
    private $moneyPrize;

    /**
     * @return mixed|void
     */
    public function execute()
    {
        $amount = $this->moneyPrize->getAmount();
        (new SendMoneyToBankAccountOperation)
            ->setAmount($amount)
            ->setBankId(42)
            ->setBankAccount(555)
            ->execute();

        $this->moneyPrize->setState(MoneyPrize::STATE_SENT);
        // todo: persist thingPrize
    }

    /**
     * @param MoneyPrize $moneyPrize
     *
     * @return SendMoneyPrizeOperation
     */
    public function setMoneyPrize(MoneyPrize $moneyPrize): SendMoneyPrizeOperation
    {
        $this->moneyPrize = $moneyPrize;
        return $this;
    }
}