<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:23
 */

namespace Lottery\Operation;


use Lottery\Entity\MoneyPrize;

class SendMoneyPrizesOperation extends AbstractOperation
{

    /** @var int how many money prizes must be processed */
    private $prizesCountToProcess = 10;

    public function execute(): void
    {
        if ($this->prizesCountToProcess === 0) {
            $limit = null; // no limit
        } else {
            $limit = $this->prizesCountToProcess;
        }

        // get waiting for sending money prizes
        /** @var MoneyPrize[] $moneyPrizes */
        $moneyPrizes = MoneyPrize::repo()->findBy([
            'state' => MoneyPrize::STATE_WAITS_FOR_SEND
        ],null,$limit);

        foreach ($moneyPrizes as $moneyPrize) {
            (new SendMoneyPrizeOperation)->setMoneyPrize($moneyPrize)->execute();
        }
    }

    /**
     * @param int $prizesCountToProcess
     *
     * @return SendMoneyPrizesOperation
     */
    public function setPrizesCountToProcess(int $prizesCountToProcess): SendMoneyPrizesOperation
    {
        $this->prizesCountToProcess = $prizesCountToProcess;
        return $this;
    }
}