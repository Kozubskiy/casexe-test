<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 0:57
 */

namespace Lottery;

class Constants
{

    public const PRIZE_PROBABILITY_THING = 50;
    public const PRIZE_PROBABILITY_MONEY = 60;
    public const PRIZE_PROBABILITY_BONUS = 70;

    public const PRIZE_MONEY_MIN = 100;
    public const PRIZE_MONEY_MAX = 1000;

    public const PRIZE_BONUS_MIN = 100;
    public const PRIZE_BONUS_MAX = 1000;

    public const MONEY_TO_BONUS_CONVERSION_RATE = 3;

}