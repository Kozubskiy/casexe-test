<?php /** @noinspection RandomApiMigrationInspection */
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 12:14
 */

namespace Helper;


class Random
{

    public static function getRandomInt(int $min,int $max): int
    {
        try {
            return \random_int($min,$max);
        } catch (\Exception $e) {
            return \rand($min,$max); // fallback
        }
    }
}