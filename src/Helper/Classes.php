<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 19:49
 */

namespace Helper;


class Classes
{

    /**
     * Returns just a class name
     * # for App\Bar\Foo\Standard returns Standard
     *
     * @param $NamespacedClassName
     *
     * @return string
     */
    public static function short( $NamespacedClassName ): string
    {

        $className = $NamespacedClassName;
        if (\is_object($NamespacedClassName)) {
            $className = \get_class( $NamespacedClassName );
        }

        $className = str_replace("\\", '/', $className);
        $className = basename( $className );
        return basename( $className );

    }

}