<?php

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\NativeFileSessionHandler;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;

/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 17:36
 */

class Context
{

    /** @var \Lottery\Logic\LotteryApp */
    private static $app;

    /** @var EntityManager */
    private static $entityManager;

    /** @var \Symfony\Component\HttpFoundation\Session\Session */
    private static $session;

    /** @var string */
    private static $rootPath;

    /**
     * @param \Lottery\Logic\LotteryApp $app
     */
    public static function setApp(\Lottery\Logic\LotteryApp $app): void
    {
        self::$app = $app;
    }

    public static function getApp(): \Lottery\Logic\LotteryApp
    {
        if (!self::$app instanceof \Lottery\Logic\LotteryApp) {
            throw new LogicException('LotteryApp is not in context');
        }
        return self::$app;
    }

    public static function getEntityManager(): EntityManager
    {
        if (!self::$entityManager instanceof EntityManager) {
            throw new LogicException('EntityManager is not in context');
        }
        return self::$entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public static function setEntityManager(EntityManager $entityManager): void
    {
        self::$entityManager = $entityManager;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    public static function getSession(): \Symfony\Component\HttpFoundation\Session\Session
    {
        if (!self::$session) {
            $storage = new NativeSessionStorage(array(), new NativeFileSessionHandler(self::getRootPath().'/var/sessions'));
            $session = new Session($storage);

            // for this moment, session should be already started everywhere where we use it
            $session->start();

            self::$session = $session;
        }

        return self::$session;
    }

    /**
     * @return string
     */
    public static function getRootPath(): string
    {
        return self::$rootPath;
    }

    /**
     * @param string $rootPath
     */
    public static function setRootPath(string $rootPath): void
    {
        self::$rootPath = $rootPath;
    }

}