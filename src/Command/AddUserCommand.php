<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:23
 */

namespace Command;


use Lottery\Operation\RegisterUserOperation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddUserCommand extends Command
{

    protected function configure()
    {

        $this
            ->setDescription('Add new user')
            ->addArgument('login',InputArgument::REQUIRED, 'Login of new user')
            ->addArgument('password',InputArgument::REQUIRED, 'Password of new user')
        ;

        parent::configure();

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $login = $input->getArgument('login');
        $password = $input->getArgument('password');
        $output->writeln('start processing user creation...');
        (new RegisterUserOperation)
            ->setLogin($login)
            ->setPassword($password)
            ->execute();
        $output->writeln('task done');
    }
}