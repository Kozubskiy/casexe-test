<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:23
 */

namespace Command;


use Lottery\Operation\SendMoneyPrizesOperation;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMoneyPrizesCommand extends Command
{

    protected function configure()
    {

        $this
            ->setDescription('Send pending won money prizes to bank accounts')
            ->addArgument('amount',InputArgument::OPTIONAL, 'How many money prizes to process (0 to send all, default 10)', 10)
        ;

        parent::configure();

    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('start processing money send...');
        $amount = (int) $input->getArgument('amount');
        (new SendMoneyPrizesOperation)
            ->setPrizesCountToProcess($amount) // get from input
            ->execute();
        $output->writeln('task done');
    }
}