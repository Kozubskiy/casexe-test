<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;


use Api\TemplateRenderer;
use Lottery\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class IndexAction extends AbstractAction
{
    public function __invoke(): Response
    {
        $user = $this->getAuthorizedUser();
        if (!$user instanceof User) {
            $content = (new TemplateRenderer)->renderPhpTemplate(TEMPLATES_PATH.'/auth.php');
            return new Response($content);
        }

        //$content = (new TemplateRenderer)->renderPhpTemplate(TEMPLATES_PATH.'/lottery.php');
        //return new Response($content);
        return $this->renderTemplateAndSend('lottery');

    }
}