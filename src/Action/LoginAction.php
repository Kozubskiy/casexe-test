<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;

use Lottery\Entity;
use Lottery\Logic\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class LoginAction extends AbstractAction
{
    public function __invoke(): Response
    {
        $login = $this->getRequest()->get('login');
        $password = $this->getRequest()->get('password');
        $user = Entity\User::repo()->findOneBy(['login'=>$login]);

        if (!$user instanceof Entity\User) {
            return new Response('Login/password pair incorrect',Response::HTTP_UNAUTHORIZED);
        }

        if (!Security::isPasswordCorrect($password,$user->getPassword())) {
            return new Response('Login/password pair incorrect',Response::HTTP_UNAUTHORIZED);
        }

        \Context::getSession()->set('userId',$user->getId());
        return new RedirectResponse('/');
        //return new Response('successfully logged in');
    }
}