<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class LogoutAction extends AbstractAction
{
    public function __invoke(): Response
    {
        \Context::getSession()->set('userId',null);
        return new RedirectResponse('/');
    }
}