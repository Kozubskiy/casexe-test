<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;

use Symfony\Component\HttpFoundation\Response;

class LotteryAction extends AbstractAction
{
    public function __invoke(): Response
    {
        return $this->renderTemplateAndSend('lottery');
    }
}