<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 17.09.18
 * Time: 18:11
 */

namespace Action;


use Symfony\Component\HttpFoundation\Response;

class PrizesAction extends AbstractAction
{

    /**
     * @return Response
     * @throws \Api\NotAuthorizedException
     */
    public function __invoke(): Response
    {
        $this->getAuthorizedUserOrFail();
        return $this->renderTemplateAndSend('prizes');
    }
}