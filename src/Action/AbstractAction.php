<?php /** @noinspection OneTimeUseVariablesInspection */

/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;


use Api\NotAuthorizedException;
use Api\TemplateRenderer;
use Lottery\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAction
{

    /** @var Request */
    private $request;

    /** @var array */
    private $params = [];

    /**
     * @throws NotAuthorizedException
     */
    protected function getAuthorizedUserOrFail(): User
    {
        $user = $this->getAuthorizedUser();
        if (!$user) {
            throw new NotAuthorizedException;
        }
        return $user;
    }

    protected function getAuthorizedUser(): ?User
    {
        $userId = (int) \Context::getSession()->get('userId');
        if ($userId === 0) {
            return null;
        }
        /** @var User $user */
        $user = User::repo()->findOneBy(['id' =>$userId]);
        return $user;
    }

    /**
     * @throws \Api\NotAuthorizedException
     * @return Response
     */
    abstract public function __invoke(): Response;

    /**
     * @param Request $request
     *
     * @return static
     */
    public function setRequest(Request $request): AbstractAction
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param array $params
     *
     * @return static
     */
    public function setParams(array $params = []): AbstractAction
    {
        $this->params = $params;
        return $this;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    protected function renderTemplate(string $templateName, array $params = []): string
    {
        if (!\array_key_exists('user',$params)) {
            $params['user'] = $this->getAuthorizedUser();
        }
        return (new TemplateRenderer)
            ->renderPhpTemplate(TEMPLATES_PATH.'/'.$templateName.'.php', $params);
    }

    protected function renderTemplateAndSend(string $templateName, array $params = []): Response
    {
        $content = $this->renderTemplate($templateName, $params);
        return new Response($content);
    }
}