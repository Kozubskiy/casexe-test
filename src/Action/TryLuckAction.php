<?php
/**
 * Created by PhpStorm.
 * User: Dmitriy V Kozubskiy (kozubskiyd@vezubr.ru, @Kozubskiy)
 * Date: 16.09.18
 * Time: 1:07
 */

namespace Action;

use Lottery\Entity;
use Lottery\Operation\ChargeBonusBalanceByBonusPrizeOperation;
use Symfony\Component\HttpFoundation\Response;

class TryLuckAction extends AbstractAction
{

    /**
     * @return Response
     * @throws \Api\NotAuthorizedException
     * @throws \Doctrine\ORM\ORMException
     */
    public function __invoke(): Response
    {
        $user = $this->getAuthorizedUserOrFail();

        $app = \Context::getApp();
        $prize = $app->getRandomePrize();

        if ($prize instanceof Entity\AbstractPrize) {
            $prize->setUser($user);
            $em = \Context::getEntityManager();
            if ($prize instanceof Entity\ThingPrize) {
                $em->persist($prize->getThing());
            }
            $em->persist($prize);
            $em->flush();
        }

        // we can charge user's bonus balance here
        if ($prize instanceof Entity\BonusPrize) {
            (new ChargeBonusBalanceByBonusPrizeOperation)
                ->setSourceBonusPrize($prize)
                ->execute();
        }

        if ($prize instanceof Entity\ThingPrize) {
            return new Response($this->renderTemplate('thing-prize',['prize' => $prize]));
        }

        if ($prize instanceof Entity\MoneyPrize) {
            return new Response($this->renderTemplate('money-prize',['prize' => $prize]));
        }

        if ($prize instanceof Entity\BonusPrize) {
            return new Response($this->renderTemplate('bonus-prize',['prize' => $prize]));
        }

        return new Response($this->renderTemplate('no-luck'));

    }
}