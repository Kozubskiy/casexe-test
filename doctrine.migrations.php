<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL. For more information, see
 * <http://www.doctrine-project.org>.
 */

use Doctrine\DBAL\Migrations\Tools\Console\Helper\ConfigurationHelper;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper;

require_once realpath(__DIR__).'/bootstrap.php';

/** @noinspection PhpUnhandledExceptionInspection */
$em = Context::getEntityManager();
$migrationsConfig = new \Doctrine\DBAL\Migrations\Configuration\Configuration( $em->getConnection() );
$migrationsConfig->setMigrationsNamespace('Lottery\Database\Migrations');
$migrationsConfig->setMigrationsDirectory(\Lottery\Database\Migrations\AbstractBaseMigration::getContainingDirectory());
$migrationsConfig->setMigrationsTableName('migration_versions');

$helperSet = new \Symfony\Component\Console\Helper\HelperSet([
    new ConnectionHelper($em->getConnection()),
    new EntityManagerHelper($em),
    new ConfigurationHelper($em->getConnection(),$migrationsConfig),
]);

$helperSet->set(new \Symfony\Component\Console\Helper\QuestionHelper(), 'question');
$cli = \Doctrine\DBAL\Migrations\Tools\Console\ConsoleRunner::createApplication($helperSet);

/** @noinspection PhpUnhandledExceptionInspection */
$cli->run();